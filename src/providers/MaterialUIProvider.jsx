import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import ThemeConfig from '../theme/index'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { useSelector } from 'react-redux'

const MaterialUIProvider = ({ children }) => {
  const { isLight } = useSelector((store) => store.theme)

  return (
    <div className={!isLight ? 'night-mode' : 'light'}>
      <ThemeConfig>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          {children}
        </LocalizationProvider>
      </ThemeConfig>
    </div>
  )
}

export default MaterialUIProvider
