import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import AlertProvider from "./providers/AlertProvider";
import GlobalFunctionsProvider from "./providers/GlobalFunctionsProvider";
import MaterialUIProvider from "./providers/MaterialUIProvider";
import Router from "./router";
import { persistor, store } from "./store";
import { QueryClientProvider } from "react-query";
import { queryClient } from "./consts/queryClients";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
} from "chart.js";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <QueryClientProvider client={queryClient}>
            <MaterialUIProvider>
              <AlertProvider>
                <GlobalFunctionsProvider />
                <BrowserRouter>
                  <Router />
                </BrowserRouter>
              </AlertProvider>
            </MaterialUIProvider>
          </QueryClientProvider>
        </PersistGate>
      </Provider>
    </div>
  );
}

export default App;
