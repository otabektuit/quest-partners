import RateCard from "../../components/RateCard";
import React from "react";

import { Box, Button, Container, Grid } from "@mui/material";
import { useState } from "react";
import { Line } from "react-chartjs-2";
import {
  _breakdownStyleTableData,
  _breakdownTableData,
  _chartData,
  _options,
  _trades
} from "../../utils/mock";
import TradeTable from "./TradeTable";

const Dashboard = () => {
  const [filter, setFilter] = useState(1);

  return (
    <Container maxWidth>
      <Grid container>
        <Grid item lg={12} mt={2}>
          <Box display="flex" gap="10px">
            <Button
              variant={filter === 1 ? "containSelected" : "outlined"}
              onClick={() => setFilter(1)}
            >
              Today
            </Button>
            <Button
              variant={filter === 30 ? "containSelected" : "outlined"}
              onClick={() => setFilter(30)}
            >
              Month to date
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Grid container columnSpacing={2} mt={2}>
        {_trades.map((trade, t) => (
          <Grid item lg={3} key={t}>
            <RateCard {...trade} />
          </Grid>
        ))}
      </Grid>
      <Grid container mt={2}>
        <Grid item lg={12}>
          <Line options={_options} data={_chartData} />
        </Grid>
      </Grid>
      <Grid container mt={2}>
        <Grid item lg={12}>
          <Line options={_options} data={_chartData} />
        </Grid>
      </Grid>
      <Grid container mt={2}>
        <Grid item lg={12}>
          <TradeTable
            columns={_breakdownTableData.columns}
            name={_breakdownTableData.name}
            data={_breakdownTableData.data}
          />
        </Grid>
      </Grid>
      <Grid container mt={2}>
        <Grid item lg={12}>
          <TradeTable
            columns={_breakdownStyleTableData.columns}
            name={_breakdownStyleTableData.name}
            data={_breakdownStyleTableData.data}
          />
        </Grid>
      </Grid>
    </Container>
  );
};

export default Dashboard;
