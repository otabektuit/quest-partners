import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Row from "../../../components/TableRow";

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      {
        date: "2020-01-05",
        customerId: "11091700",
        amount: 3
      },
      {
        date: "2020-01-02",
        customerId: "Anonymous",
        amount: 1
      }
    ]
  };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0, 3.99),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3, 4.99),
  createData("Eclair", 262, 16.0, 24, 6.0, 3.79),
  createData("Cupcake", 305, 3.7, 67, 4.3, 2.5),
  createData("Gingerbread", 356, 16.0, 49, 3.9, 1.5)
];

export default function TradeTable({ name, columns, data, hideColumns }) {
  return (
    <TableContainer component={Paper}>
      <Typography variant="h6" gutterBottom component="div">
        {name}
      </Typography>
      <Table aria-label="collapsible table">
        {!hideColumns && (
          <TableHead>
            <TableRow>
              <TableCell />
              {columns?.map((column, c) => (
                <TableCell key={c}>{column.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
        )}

        <TableBody>
          {data?.map((row) => (
            <Row key={row.name} row={row} columns={columns} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
