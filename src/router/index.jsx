import { Navigate, Route, Routes } from "react-router-dom";
import Layout from "../components/Layout";
import Dashboard from "../pages/Dashboard";

const Router = () => {
  // const isAuth = useSelector((state) => state.auth.isAuth);

  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route path=":id" element={<Dashboard />} />
      </Route>
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default Router;
