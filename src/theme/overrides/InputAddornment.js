// ----------------------------------------------------------------------

export default function InputAdornment(theme) {
  return {
    MuiInputAddornment: {
      styleOverrides: {
        root: {
          minWidth: '44!important',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        },
        positionStart: {
          minWidth: '44!important',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }
      }
    }
  };
}
