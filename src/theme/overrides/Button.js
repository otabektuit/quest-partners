// ----------------------------------------------------------------------

export default function Button(theme) {
  return {
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: "16px",
          lineHeight: "24px",
          textTransform: "none",
          boxShadow: "none!important",
          transition: "all 0.25s ease-in-out",
          "&:hover": {
            transform: "translateY(-1px)"
          },
          borderRadius: "12px",
          padding: "10px 20px",
          minWidth: "auto",
          whiteSpace: "nowrap",
          border: `1px solid transparent`
        },
        sizeLarge: {
          height: 48
        },
        colorInherit: {
          backgroundColor: theme.palette.primary.grey,
          color: theme.palette.primary.text,
          "&:hover": {
            backgroundColor: theme.palette.primary.grey
          }
        },
        textInfo: {
          backgroundColor: theme.palette.info.main,
          color: theme.palette.secondary.main,
          "&:hover": {
            backgroundColor: theme.palette.info.main
          }
        },
        containedPrimary: {
          "&:hover": {
            backgroundColor: theme.palette.primary.main
          }
        },
        activeButton: {
          backgroundColor: "rgba(0,0,0,0.7)",
          color: "#fff"
        },
        outlined: {
          border: `1px solid ${theme.palette.primary.main}`
        },
        containSelected: {
          backgroundColor: theme.palette.primary.main + "!important",
          color: "#fff"
        }
      }
    }
  };
}
