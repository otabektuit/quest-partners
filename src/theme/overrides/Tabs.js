// ----------------------------------------------------------------------

import { padding } from '@mui/system'

export default function Tabs(theme) {
  return {
    MuiTabs: {
      styleOverrides: {
        root: {
          alignItems: 'center'
        },
        indicator: {
          height: '8px',
          borderRadius: '8px 8px 0px 0px'
        }
      }
    },
    MuiTab: {
      styleOverrides: {
        root: {
          padding: 0,
          fontSize: '16px',
          lineHeight: '24px',
          color: theme.isDark ? '#fff!important' : '#949CB2',
          '&.Mui-selected': {
            color: '#323232',
            '& .MuiTab-iconWrapper': {
              background: theme.palette.primary.main,
              color: '#fff'
            }
          },
          '&:not(:last-child)': {
            marginRight: '32px'
          },
          minWidth: 'auto',
          minHeight: '72px'
        },
        labelIcon: {
          minHeight: 72,
          paddingTop: 0,
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'row-reverse',
          columnGap: '12px',
          marginBottom: 0
        },

        wrapper: {
          flexDirection: 'row',
          whiteSpace: 'nowrap'
        },
        textColorInherit: {
          opacity: 1,
          color: theme.palette.text.secondary
        }
      }
    },
    MuiTabPanel: {
      styleOverrides: {
        root: {
          padding: 0
        }
      }
    },
    MuiTabScrollButton: {
      styleOverrides: {
        root: {
          width: 30,
          borderRadius: '50%',
          height: 30,
          '& .MuiSvgIcon-root': {
            width: 30,
            height: 30
          },
          padding: '0 10px'
        }
      }
    }
  }
}
