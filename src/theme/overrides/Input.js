// ----------------------------------------------------------------------

export default function Input(theme) {
  return {
    MuiInputBase: {
      styleOverrides: {
        root: {
          backgroundColor: theme.isDark ? '#1C1C24!important' : '#F3F4F6'
        },
        input: {
          '&::placeholder': {
            opacity: 1,
            color: theme.palette.text.disabled
          }
        }
      }
    },
    MuiInput: {
      styleOverrides: {
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey[500_56]
          }
        }
      }
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: theme.palette.grey[500_12],
          '&:hover': {
            backgroundColor: theme.palette.grey[500_16]
          },
          '&.Mui-focused': {
            backgroundColor: theme.palette.action.focus
          },
          '&.Mui-disabled': {
            backgroundColor: theme.palette.action.disabledBackground
          }
        },
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey[500_56]
          }
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        input: {
          padding: '12px 16px',
          height: 'auto',
          '&::placeholder': {
            color: '#8891AA'
          },
          '@media (max-width: 600px)': {
            padding: '8px 16px'
          }
        },
        root: {
          transition: '0.4s ease-in-out all',
          background: theme.palette.grey[200],
          fontSize: '16px',
          lineHeight: '24px',
          borderRadius: '12px',
          position: 'relative',
          '&:hover': {
            '& .MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.primary.main
            }
          },
          '& .MuiInputAdornment-root': {
            minWidth: '55px',
            '& p': {
              height: '100%',
              padding: '11px 20px 10px',
              background: theme.isDark ? '#535865' : theme.palette.grey[300],
              display: 'flex',
              alignItems: 'center',
              color: theme.isDark ? '#fff' : '#010001',
              transition: '0.4s ease-in-out all',
              position: 'absolute'
            }
          },
          '& .MuiInputAdornment-positionStart': {
            '& p': {
              left: '0',
              borderRadius: '12px 0 0 12px'
            }
          },
          '& .MuiInputAdornment-positionEnd': {
            '& p': {
              right: '0',
              borderRadius: '0 12px 12px 0'
            }
          },
          '& .MuiOutlinedInput-notchedOutline': {
            transition: 'all 0.25s ease-in-out',
            borderWidth: '1px!important',
            borderColor: theme.isDark ? '#495065' : theme.palette.grey[100]
          },
          '&.Mui-disabled': {
            '& .MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.action.disabledBackground
            }
          }
        }
      }
    }
  }
}
