// ----------------------------------------------------------------------

export default function Radio(theme) {
  return {
    MuiRadio: {
      defaultProps: {
        color: 'primary'
      },

      styleOverrides: {
        root: {
          padding: 0,
          marginRight: 12,
          with: 24,
          height: 24,
          color: theme.palette.primary.main,
          '& svg': {
            color: theme.palette.primary.main
          }
        }
      }
    }
  }
}
